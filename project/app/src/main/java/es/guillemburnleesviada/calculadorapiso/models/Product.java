package es.guillemburnleesviada.calculadorapiso.models;

public class Product {

    private String name = "";
    private float price = 0;
    private boolean shiru;
    private boolean pilar;
    private boolean joel;
    private boolean clara;
    private float lastShared;

    public Product() {
    }

    public Product(String name, float price, boolean shiru, boolean pilar, boolean joel, boolean clara) {
        this.name = name;
        this.price = price;
        this.shiru = shiru;
        this.pilar = pilar;
        this.joel = joel;
        this.clara = clara;
    }

    public Product(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public float getLastShared() {
        return lastShared;
    }

    public void setLastShared(float lastShared) {
        this.lastShared = lastShared;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isShiru() {
        return shiru;
    }

    public void setShiru(boolean shiru) {
        this.shiru = shiru;
    }

    public boolean isPilar() {
        return pilar;
    }

    public void setPilar(boolean pilar) {
        this.pilar = pilar;
    }

    public boolean isJoel() {
        return joel;
    }

    public void setJoel(boolean joel) {
        this.joel = joel;
    }

    public boolean isClara() {
        return clara;
    }

    public void setClara(boolean clara) {
        this.clara = clara;
    }
}
