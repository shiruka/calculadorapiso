package es.guillemburnleesviada.calculadorapiso;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.guillemburnleesviada.calculadorapiso.databinding.ActivityMainBinding;
import es.guillemburnleesviada.calculadorapiso.databinding.AlertViewBinding;
import es.guillemburnleesviada.calculadorapiso.databinding.ContentMainBinding;
import es.guillemburnleesviada.calculadorapiso.models.Product;
import es.guillemburnleesviada.calculadorapiso.utils.Utils;

public class MainActivity extends AppCompatActivity implements ViewBinding {

    private ActivityMainBinding bindingMain;

    private float shiru = 0;
    private float pilar = 0;
    private float joel = 0;
    private float clara = 0;
    private float total = 0;

    private Product lastProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingMain = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(bindingMain.getRoot());
        setSupportActionBar(bindingMain.toolbar);

        bindingMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProduct().show();
            }
        });

        bindingMain.contentmain.btnClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAll();
            }
        });

        bindingMain.contentmain.btnClearLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lastProduct.isShiru()) {
                    shiru -= lastProduct.getLastShared();
                }

                if (lastProduct.isPilar()) {
                    pilar -= lastProduct.getLastShared();
                }

                if (lastProduct.isJoel()) {
                    joel -= lastProduct.getLastShared();
                }

                if (lastProduct.isClara()) {
                    clara -= lastProduct.getLastShared();
                }

                if (total>0) {
                    total -= lastProduct.getPrice();
                }

                lastProduct = null;

                bindingMain.contentmain.txtLastItemName.setText("Na");
                updateText();
                bindingMain.contentmain.btnClearLast.setEnabled(false);

            }
        });

    }

    private void updateText() {
        bindingMain.contentmain.txtTotalCount.setText(String.format("%.3f", total));
        bindingMain.contentmain.txtShiruCount.setText(String.format("%.3f", shiru));
        bindingMain.contentmain.txtPilarCount.setText(String.format("%.3f", pilar));
        bindingMain.contentmain.txtJoelCount.setText(String.format("%.3f", joel));
        bindingMain.contentmain.txtClaraCount.setText(String.format("%.3f", clara));
    }

    private void clearAll() {
        shiru = 0;
        pilar = 0;
        joel = 0;
        clara = 0;
        total = 0;
        lastProduct = null;

        updateText();
        bindingMain.contentmain.txtLastItemName.setText("Na");
        bindingMain.contentmain.btnClearLast.setEnabled(false);

    }

    private AlertDialog addProduct() {

        bindingMain.contentmain.btnClearLast.setEnabled(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View alert = inflater.inflate(R.layout.alert_view, null);
        final AlertViewBinding alertBinding = AlertViewBinding.bind(alert);

        final EditText txtProductName = alert.findViewById(R.id.txtProductName);
        final EditText txtProductPrice = alert.findViewById(R.id.txtProductPrice);

        builder.setView(alert);

        builder.setTitle(R.string.text_addProduct);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                try {
                    if (!txtProductName.getText().toString().isEmpty() || !txtProductPrice.getText().toString().isEmpty()){
                        Product product = new Product(txtProductName.getText().toString(),
                                Float.parseFloat(txtProductPrice.getText().toString()),
                                alertBinding.cbShiru.isChecked(),
                                alertBinding.cbPilar.isChecked(),
                                alertBinding.cbJoel.isChecked(),
                                alertBinding.cbClara.isChecked());

                        String[][] result = Utils.calculateProduct(product);

                        total += product.getPrice();
                        shiru += Float.parseFloat(result[0][1]);
                        pilar += Float.parseFloat(result[1][1]);
                        joel += Float.parseFloat(result[2][1]);
                        clara += Float.parseFloat(result[3][1]);

                        bindingMain.contentmain.txtLastItemName.setText(product.getName());
                        updateText();

                        lastProduct = product;
                        lastProduct.setLastShared(Float.parseFloat(result[4][1]));
                    }else{
                        Toast.makeText(MainActivity.this, "Missing input", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        });

        return builder.create();

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
